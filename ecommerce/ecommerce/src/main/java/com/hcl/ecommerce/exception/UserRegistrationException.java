package com.hcl.ecommerce.exception;

public class UserRegistrationException extends Exception {

	private static final long serialVersionUID = 1L;

	public UserRegistrationException(String exception) {

		super(exception);
	}

}
