package com.hcl.ecommerce.constants;

public class ApplicationConstants {
	private ApplicationConstants() {

	}
	
	public static final Integer SUCCESS_CODE = 607;
	public static final Integer UNSUCCESS_CODE = 608;
	public static final String INVALID_DETAILS = "PLEASE ENTER VALID DETAILS!!!";
	public static final String NOT_REGISTERED = "USER NOT REGISTERED!!!";
	public static final String PRODUCTS_NOT_AVAILABLE = "PRODUCTS NOT AVAILABLE!!!";
	public static final String PRODUCT_NOT_AVAILABLE_FOR_REVIEW = "PRODUCT NOT AVAILABLE FOR REVIEW!!!";
	public static final String REVIEW_ADDED_SUCCESSFULLY = "REVIEW ADDED SUCCESSFULLY!!!";
	public static final String USER_PRESENT_ALREADY = "USER IS ALREADY PRESENT!!!";
	public static final String SELECT_PRIORITY = "SELECT TYPE OF PRIORITY!!!";
	public static final String USER_REGISTERED = "USER REGISTERED!!!";
	public static final String NO_REVIEW = "NO REVIEWS AVAILABLE!!!";
	public static final String PRODUCT_ADDED_SUCCESSFULLY = "PRODUCT ADDED SUCCESSFULLY!!!";
	public static final String USER_NOT_ADMIN = "USER IS NOT AN ADMIN!!!";
	public static final String ADMIN_NOT_REGISTERED = "ADMIN IS NOT REGISTERED!!!";
	public static final String QUANTITY_NOT_AVAILABLE = "QUANTITY IS MORE THAN AVAILABILITY!!!";
	public static final String PURCHASED_PRODUCT_NOT_AVAILABLE_FOR_REVIEW = "USER HAS NOT PURCHASED ANY PRODUCT";
	public static final String USER_NOT_ALLOWED_TO_BUY = "ADMIN IS NOT ALLOWED TO BUY PRODUCT!!!";
}
