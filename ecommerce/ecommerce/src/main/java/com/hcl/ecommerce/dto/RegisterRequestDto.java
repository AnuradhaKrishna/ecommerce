package com.hcl.ecommerce.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RegisterRequestDto {
	@NotEmpty(message = "Name should not be empty")
	private String name;
	@NotEmpty(message = "Password should not be empty")
	private String password;
	@NotEmpty(message = "Email should not be empty")
	@Email(message = "Email should be valid")
	private String emailId;
	private Integer typeId;

}
