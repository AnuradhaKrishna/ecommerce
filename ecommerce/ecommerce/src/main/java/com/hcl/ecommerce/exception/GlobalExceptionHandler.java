package com.hcl.ecommerce.exception;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.hcl.ecommerce.constants.ApplicationConstants;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	@ExceptionHandler(UserRegistrationException.class)
	ResponseEntity<ErrorMessage> userRegistrationException(UserRegistrationException userRegistrationException) {
		ErrorMessage errorResponse = new ErrorMessage();
		errorResponse.setMessage(userRegistrationException.getMessage());
		errorResponse.setStatus(ApplicationConstants.UNSUCCESS_CODE);
		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(ProductException.class)
	ResponseEntity<ErrorMessage> productException(ProductException productException) {
		ErrorMessage errorResponse = new ErrorMessage();
		errorResponse.setMessage(productException.getMessage());
		errorResponse.setStatus(ApplicationConstants.UNSUCCESS_CODE);
		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(SearchProductException.class)
	ResponseEntity<ErrorMessage> searchProductException(SearchProductException searchProductException) {
		ErrorMessage errorResponse = new ErrorMessage();
		errorResponse.setMessage(searchProductException.getMessage());
		errorResponse.setStatus(ApplicationConstants.UNSUCCESS_CODE);
		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(ReviewException.class)
	ResponseEntity<ErrorMessage> reviewException(ReviewException reviewException) {
		ErrorMessage errorResponse = new ErrorMessage();
		errorResponse.setMessage(reviewException.getMessage());
		errorResponse.setStatus(ApplicationConstants.UNSUCCESS_CODE);
		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
	}

	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body,
			org.springframework.http.HttpHeaders headers, HttpStatus status, WebRequest request) {
		if (ex instanceof MethodArgumentNotValidException) {
			MethodArgumentNotValidException exception = (MethodArgumentNotValidException) ex;
			List<String> errorList = exception.getBindingResult().getFieldErrors().stream()
					.map(fieldError -> fieldError.getDefaultMessage()).collect(Collectors.toList());
			InputErrorMessage errorDetails = new InputErrorMessage("THIS IS THE CUSTOM EXCEPTION HANDLER MESSAGE", errorList,
					ApplicationConstants.UNSUCCESS_CODE);
			return super.handleExceptionInternal(ex, errorDetails, headers, status, request);
		}
		return super.handleExceptionInternal(ex, body, headers, status, request);
	}

}
