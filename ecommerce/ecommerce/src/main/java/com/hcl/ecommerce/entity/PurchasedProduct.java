package com.hcl.ecommerce.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
public class PurchasedProduct {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer purchasedProductId;
	private Integer userId;
	private Integer productId;
	private Integer quantity;
	private LocalDateTime purchasedDate;
	private Double price;
	

}
