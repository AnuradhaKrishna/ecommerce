package com.hcl.ecommerce.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hcl.ecommerce.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer>{
	
	@Query(value="select * from Product where product_name like %:productName%", nativeQuery=true)
	List<Product> findByProductName(String productName);
	
	Optional<List<Product>> findByTypeId(Integer typeId);

	Optional<Product> findByProductIdAndProductName(Integer productId, String productName);

	Optional<Product> findByProductId(Integer productId);

	

}
