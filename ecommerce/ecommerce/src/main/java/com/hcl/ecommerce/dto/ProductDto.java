package com.hcl.ecommerce.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProductDto {
	
	private Integer productId;
	private String productName;
	private Double price;

}
