package com.hcl.ecommerce.service;

import org.springframework.stereotype.Service;

import com.hcl.ecommerce.dto.RegisterRequestDto;
import com.hcl.ecommerce.dto.ResponseDto;
import com.hcl.ecommerce.exception.UserRegistrationException;

@Service
public interface RegisterService {

	ResponseDto register(RegisterRequestDto registerRequestDto) throws UserRegistrationException;

}
