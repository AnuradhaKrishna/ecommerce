package com.hcl.ecommerce.exception;

public class ReviewException extends Exception {
	private static final long serialVersionUID = 1L;

	public ReviewException(String exception) {

		super(exception);
	}
}
