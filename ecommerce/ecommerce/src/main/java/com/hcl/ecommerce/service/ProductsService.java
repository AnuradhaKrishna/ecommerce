package com.hcl.ecommerce.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.hcl.ecommerce.dto.ProductsDto;
import com.hcl.ecommerce.dto.PurchasedProductRequestDto;
import com.hcl.ecommerce.dto.PurchasedProductResponseDto;
import com.hcl.ecommerce.dto.ResponseDto;
import com.hcl.ecommerce.dto.ReviewRequestDto;
import com.hcl.ecommerce.dto.SearchResponseDto;
import com.hcl.ecommerce.exception.ProductException;
import com.hcl.ecommerce.exception.ReviewException;
import com.hcl.ecommerce.exception.SearchProductException;
import com.hcl.ecommerce.exception.UserRegistrationException;

@Service
public interface ProductsService {
	
	public List<SearchResponseDto> search(String productName) throws SearchProductException;
	
	public ResponseDto review(ReviewRequestDto reviewRequestDto) throws ReviewException;

	ResponseDto addProducts(ProductsDto productDtos) throws ProductException;

	PurchasedProductResponseDto buyProduct(PurchasedProductRequestDto purchasedProductRequestDto)
			throws UserRegistrationException;

}
