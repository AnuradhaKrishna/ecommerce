package com.hcl.ecommerce.exception;

public class SearchProductException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public SearchProductException(String exception) {

		super(exception);
	}

}
