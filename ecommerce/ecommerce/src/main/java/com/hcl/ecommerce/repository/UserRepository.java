package com.hcl.ecommerce.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.ecommerce.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

	Optional<User> findByTypeId(Integer typeId);

	Optional<User> findByEmailId(String emailId);

	Optional<User> findByEmailIdAndPassword(String emailId, String password);

	Optional<User> findByUserId(Integer userId);

}
