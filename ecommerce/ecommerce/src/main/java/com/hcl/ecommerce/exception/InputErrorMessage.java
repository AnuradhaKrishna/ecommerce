package com.hcl.ecommerce.exception;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InputErrorMessage {

	private String message;
	private int status;
	private List<String> errorMessage;

	public InputErrorMessage(String message, List<String> errorMessage, int status) {
		this.message = message;
		this.status = status;
		this.errorMessage = errorMessage;
	}

	public InputErrorMessage() {
		super();
	}
}
