package com.hcl.ecommerce.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.ecommerce.entity.PurchasedProduct;

@Repository
public interface PurchasedProductRepository extends JpaRepository<PurchasedProduct, Integer>{

	Optional<PurchasedProduct> findByPurchasedProductId(Integer purchasedProductId);
	
	List<PurchasedProduct> findByProductIdIn(List<Integer> productList);
	
//	List<PurchasedProduct> findByProductId(List<Integer> productList);

}
