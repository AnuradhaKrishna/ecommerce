package com.hcl.ecommerce.service;

import static org.junit.Assert.assertNotNull;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.hcl.ecommerce.dto.RegisterRequestDto;
import com.hcl.ecommerce.dto.ResponseDto;
import com.hcl.ecommerce.entity.User;
import com.hcl.ecommerce.exception.UserRegistrationException;
import com.hcl.ecommerce.repository.UserRepository;

@RunWith(MockitoJUnitRunner.Silent.class)
public class RegisterServiceImplTest {

	@InjectMocks
	RegisterServiceImpl userServiceImpl;

	@Mock
	UserRepository userRepository;
	
	@Test
	public void registerservicetests() throws UserRegistrationException {
		RegisterRequestDto registerRequestDto = new RegisterRequestDto();
		registerRequestDto.setName("Haran");
		registerRequestDto.setEmailId("str@gmail.com");
		registerRequestDto.setPassword("haran");
		registerRequestDto.setTypeId(0);

		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage("Valid");
		responseDto.setStatusCode(200);

		User user = new User();
		user.setUserId(1);
		user.setEmailId("str@gmail.com");
		user.setName("Haran");
		user.setTypeId(0);
		user.setPassword("haran");

		Mockito.when(userRepository.save(user)).thenReturn(user);
		ResponseDto result = userServiceImpl.register(registerRequestDto);
		assertNotNull(result);
	}

	@Test(expected = UserRegistrationException.class)
	public void registerservicetest() throws UserRegistrationException {
		RegisterRequestDto registerRequestDto = new RegisterRequestDto();
		registerRequestDto.setName("Haran");
		registerRequestDto.setEmailId("str@gmail.com");
		registerRequestDto.setPassword("haran");
		registerRequestDto.setTypeId(0);

		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage("Valid");
		responseDto.setStatusCode(200);

		User user = new User();
		user.setUserId(1);
		user.setEmailId("str@gmail.com");
		user.setName("Haran");
		user.setTypeId(0);
		user.setPassword("haran");

		Mockito.when(userRepository.findByEmailId(Mockito.anyString())).thenReturn(Optional.of(user));
		ResponseDto result = userServiceImpl.register(registerRequestDto);
		assertNotNull(result);
	}
	
	@Test(expected = UserRegistrationException.class)
	public void registerservicetest1() throws UserRegistrationException {
		RegisterRequestDto registerRequestDto = new RegisterRequestDto();
		registerRequestDto.setName("Haran");
		registerRequestDto.setEmailId("str@gmail.com");
		registerRequestDto.setPassword("haran");
		registerRequestDto.setTypeId(3);

		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage("Valid");
		responseDto.setStatusCode(200);

		User user = new User();
		user.setUserId(1);
		user.setEmailId("str@gmail.com");
		user.setName("Haran");
		user.setTypeId(0);
		user.setPassword("haran");

		Mockito.when(userRepository.findByEmailId(Mockito.anyString())).thenReturn(Optional.ofNullable(null));
		userServiceImpl.register(registerRequestDto);
		
	}

}
